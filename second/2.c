#include <stdio.h>

int main(void)
{
    printf("%d\n", sizeof(char));
    printf("%d\n", sizeof(short int));
    printf("%d\n", sizeof(int));
    printf("%d\n", sizeof(long int));

    printf("%d\n", sizeof(unsigned char));
    printf("%d\n", sizeof(unsigned short int));
    printf("%d\n", sizeof(unsigned int));
    printf("%d\n", sizeof(unsigned long int));
    return 0;
}
